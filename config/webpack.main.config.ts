import { Configuration } from "webpack";
import { config, isProduction } from "./webpack.config";

let devPlugins = [];

if (isProduction) {
  devPlugins = [];
}
const ElectronConfig = Object.assign<Configuration, Configuration>(config, {
  plugins: [...devPlugins],
});

export default ElectronConfig;
