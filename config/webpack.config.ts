import CleanWebpackPlugin from "clean-webpack-plugin";
import { resolve } from "path";
import TerserPlugin from "terser-webpack-plugin";
import { Configuration, EnvironmentPlugin } from "webpack";

const NODE_ENV = process.env.NODE_ENV as Configuration["mode"];
export const isProduction = NODE_ENV === "production";
export const isNotProduction = !isProduction;
export const isDevelopment = !isProduction;

let devPlugins = [];

if (isProduction) {
  devPlugins = [];
}

export const config: Configuration = {
  entry: {
    main: "./electron/main.ts",
    preload: "./electron/preload.js",
  },
  target: "electron-main",
  mode: NODE_ENV,
  watch: isNotProduction,
  context: resolve(__dirname, "../src"),
  output: {
    chunkFilename: "[name].js",
    path: resolve(__dirname, "../build"),
  },
  optimization: {
    minimize: isProduction,
    minimizer: [new TerserPlugin()],
  },
  stats: {
    all: false,
    chunks: true,
    entrypoints: true,
    errorDetails: true,
    errors: true,
    performance: true,
    timings: true,
  },
  module: {
    rules: [
      {
        exclude: /node_modules/,
        loaders: ["babel-loader", "ts-loader"],
        test: /\.tsx?$/,
      },
      {
        exclude: /node_modules/,
        loaders: ["babel-loader"],
        test: /\.jsx?$/,
      },
    ],
  },
  devtool: isNotProduction ? "source-map" : false,
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
  },
  plugins: [
    new CleanWebpackPlugin(),
    new EnvironmentPlugin({
      NODE_ENV,
    }),
    ...devPlugins,
  ],
};
