import HTMLWebpackPlugin from "html-webpack-plugin";
import { resolve } from "url";
import { Configuration } from "webpack";
import { config, isProduction } from "./webpack.config";

const RendererConfig: Configuration = Object.assign<
  Configuration,
  Configuration
>(config, {
  entry: {
    renderer: "./renderer/index.ts",
  },
  output: {
    path: resolve(__dirname, "./build/renderer"),
  },
  devServer: {
    hot: true,
    port: 55555,
  },
  target: isProduction ? "electron-renderer" : "web",
  plugins: [new HTMLWebpackPlugin({ title: "Torrento" })],
});

export default RendererConfig;
