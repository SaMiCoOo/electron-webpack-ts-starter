interface SafeIpcRenderer {
  on: (channel: string, ...args: any[]) => void;
  once: (channel: string, ...args: any[]) => void;
  removeListener: (channel: string, ...args: any[]) => void;
  removeAllListeners: (channel: string, ...args: any[]) => void;
  send: (channel: string, ...args: any[]) => void;
  sendSync: (channel: string, ...args: any[]) => void;
  sendToHost: (channel: string, ...args: any[]) => void;
}
interface Window {
  ipc: SafeIpcRenderer;
  x: number;
}
