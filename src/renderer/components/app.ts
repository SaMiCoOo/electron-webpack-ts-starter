import { customElement, html, LitElement, property } from "lit-element";
import { repeat } from "lit-html/directives/repeat";

@customElement("x-app")
export default class App extends LitElement {
  @property()
  public messages: string[];
  constructor() {
    super();
    this.messages = [];
    window.ipc.on("pong", (_: any, args: string) => this.addMessage(args));
  }
  public render() {
    return html`
      <h2>Hi</h2>
      <button @click="${this.ping}">Ping</button>
      <div>
        ${repeat(
          this.messages,
          m => m,
          message =>
            html`
              <h5>${message}</h5>
            `
        )}
      </div>
    `;
  }
  private addMessage(msg: string) {
    this.messages = [...this.messages, msg];
  }

  private ping() {
    window.ipc.send("ping");
  }
}
