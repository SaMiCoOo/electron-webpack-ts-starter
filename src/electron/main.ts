// Modules to control application life and create native browser window
import { app, BrowserWindow, ipcMain, IpcMainEvent } from "electron";
import { resolve } from "path";
import { format as formatURL } from "url";
import { reloadOnChange } from "./utils/watcher";

const mode = process.env.NODE_ENV;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow: BrowserWindow;

async function createWindow() {
  const preload =
    mode === "production"
      ? resolve(app.getAppPath(), "build/preload.js")
      : resolve("build/preload.js");
  // Create the browser window.
  mainWindow = new BrowserWindow({
    title: "Torrento",
    show: false,
    width: 800,
    webPreferences: {
      preload,
      sandbox: true,
      nodeIntegration: false,
      contextIsolation: false,
    },
  });

  // initialize the file watcher
  const watcher = reloadOnChange(mainWindow);

  // and load the index.html of the app.
  try {
    if (process.env.NODE_ENV === "development") {
      await mainWindow.loadURL("http://localhost:55555");
    } else {
      await mainWindow.loadURL(
        formatURL({
          pathname: resolve(app.getAppPath(), "build/renderer/index.html"),
          protocol: "file:",
          slashes: true,
        })
      );
    }

    mainWindow.showInactive();
    // Open the DevTools.
    mainWindow.webContents.openDevTools();

    // Emitted when the window is closed.
    mainWindow.on("closed", () => {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      watcher.close();
      mainWindow.destroy();
    });
  } catch (e) {
    mainWindow.close();
    app.exit();
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") app.quit();
});

app.on("activate", async () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow.isDestroyed()) {
    await createWindow();
  }
});

ipcMain.on("ping", (event: IpcMainEvent) => {
  event.preventDefault();
  event.sender.send(
    "pong",
    Math.random()
      .toString(36)
      .replace(/[^a-z]+/g, "")
      .substr(0, 5)
  );
});
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
