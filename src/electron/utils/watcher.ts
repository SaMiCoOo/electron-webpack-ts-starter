import { BrowserWindow } from "electron";
import { watch } from "fs";
import { resolve } from "path";

export function reloadOnChange(win: BrowserWindow) {
  if (process.env.NODE_ENV !== "development") return { close: () => null };

  const watcher = watch(
    resolve("build"),
    { encoding: "buffer", recursive: true },
    () => {
      win.reload();
    }
  );
  return watcher;
}
